<?php

    function setDecimalFromBinary($number){
        $number = strrev($number);
        $number_arr = str_split($number);
        $error_msg = "Podano liczbę w niezgodnym systemie";
        foreach ($number_arr as $value) {
            if ($value > 1) {
                return $error_msg;
            }
        }
        $len = sizeof($number_arr) - 1;
        $result = 0;

        for ($i = $len; $i >= 0; $i--) {
            $result += $number_arr[$i]*pow(2, $i);
        }
        return $result."<sub>(10)</sub>";
    }

    function setBinary($number){
        $new_num = "";
        $number = (int)$number;
        if ($number == 0) {
            return "0";
        }
        if ($number < 0) {
            return "Brak obsługi liczb ujemnych";
        }
        while($number >= 1) {
            if ($number % 2 == 0) {
                $new_num.="0";
            } else {
                $new_num.="1";
            }
            $number /= 2;
        }
        return strrev($new_num)."<sub>(2)</sub>";
    }

    function setHexa($number){
        $hexNum = "";
        $number = (int)$number;
        $elems = str_split("0123456789ABCDEF");
        if ($number == 0) {
            return "0<sub>(16)</sub>";
        }
        while ($number >= 1) {
            $y = $number % 16;
            $hexNum = $elems[$y].$hexNum;
            $number /= 16;
        }
        return $hexNum."<sub>(16)</sub>";
    }

    function setDecimalFromHexa($number) {
        return hexdec($number)."<sub>(10)</sub>";
    }

?>
