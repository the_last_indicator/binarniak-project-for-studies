<?php
    require_once("functions.php");
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Tytul</title>
    <link rel="stylesheet" type="text/css" href="styles/style.css">
</head>
<body>
    <div class="container">
        <nav>
            <div class="navbar">
                <div class="left-logo">
                    <a href="index.html">Binarniak</a><span class="dotcom">++</span>
                </div>
                <div class="sub-logo">Super-fajny kalkulatorek</div>
                <div style="clear:both;"></div>
            </div>

            <div class="menu">
                <ul>
                 <li><a href="index.html">Strona główna</a></li>
                 <li><a href="binar.php">Kalkulator binarny</a></li>
                 <li><a href="hexa.php">Kalkulator heksadecymalny</a></li>
               </ul>
            </div>
        </nav>

        <section>
            <div class="section">
                <p>Kalkurator heksadecymalny</p>
            </div>
            <form method="post">
                <h4>W jakim systemie wprowadzasz liczbę?</h4>
                <label for="hexa">szesnastkowym</label>
                <input type="radio" id="hexa" value="hexa" name="choose">
                &nbsp;
                <label for="decimal">dziesiętnym</label>
                <input type="radio" id="decimal" value="decimal" name="choose">
                <h4>Podaj liczbę w wybranym systemie</h4>
                <p class="info">przy wyborze szesnastkowym wprowadzone znaki spoza systemu są ignorowane, np. <br />
                122ASD traktowane jest jak 122AD</p>
                <input type="text" name="number">&nbsp;
                <button type="submit">Przelicz</button>
            </form>
            <br /><hr />

            <?php
                if (isset($_POST["number"])) {
                    if (@$_POST['choose'] == "decimal"){
                        echo "Podano: ".$_POST["number"]."<sub>(10)</sub>";
                        echo "<br /><br />";
                        echo "<b>Wynik: ".setHexa($_POST["number"])."</b><hr />";
                    } elseif (@$_POST['choose'] == "hexa"){
                        echo "Podano: ".$_POST["number"]."<sub>(16)</sub>";
                        echo "<br /><br />";
                        echo "<b>Wynik: ".setDecimalFromHexa($_POST["number"])."</b><hr />";
                    }  else {
                        echo "Wybierz system liczbowy!";
                    }
                }
            ?>
        </section>

    </div>

    <footer>
        <div>
            <a href="index.html">Binarniak</a>++ &nbsp; &copy; Daniel Gorzka 2019
        </div>
    </footer>

</body>
</html>
